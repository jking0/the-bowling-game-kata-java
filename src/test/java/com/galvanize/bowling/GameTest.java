package com.galvanize.bowling;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameTest {
//    Can create a new game - Score should be zero
    @Test
    public void canCreateNewGame(){
        Game game = new Game();
        assertEquals(0, game.getScore());
    }

//    Can record rolling of ball - Should accept an integer for the number of pins knocked down in that roll
    @Test
    public void canRecordRoll(){
        Game game = new Game();
        game.roll(7);
        assertEquals(7, game.getScore());
    }

//    Can score a game of all (20) gutters - Score of zero
    @Test
    public void rollAllGutters(){
        Game game = new Game();
        for(int i = 0; i < 20; i++){
            game.roll(0);
        }
        assertEquals(0, game.getScore());
    }

//    Can score a game of all (20) ones - Score of 20
    @Test
    public void rollAllOnes(){
        Game game = new Game();
        for(int i = 0; i < 20; i++){
            game.roll(1);
        }
        assertEquals(20, game.getScore());
    }

//    Can score a game with one spare (say two fives), followed by one roll with three pins down, and the rest (17) gutters - Score of 16
    @Test
    public void rollSpare(){
        Game game = new Game();
        game.roll(5);
        game.roll(5);
        game.roll(3);
        for(int i = 0; i < 17; i++){
            game.roll(0);
        }
        assertEquals(16, game.getScore());
    }

//    Can score a game with one strike, followed by two rolls with three pins down each, and the rest (16) gutters - Score of 22
    @Test
    public void rollStrike(){
        Game game = new Game();
        game.roll(10);
        game.roll(3);
        game.roll(3);
        for(int i = 0; i < 16; i++){
            game.roll(0);
        }
        assertEquals(22, game.getScore());
    }
//    Can score a perfect game - ten strikes + two bonus 10s in the tenth frame - Score of 300
    @Test
    public void rollPerfectGame(){
        Game game = new Game();
        for(int i = 0; i < 12; i++){
            game.roll(10);
        }
        assertEquals(300, game.getScore());
    }
}