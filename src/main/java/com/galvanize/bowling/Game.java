package com.galvanize.bowling;

public class Game {
    private int[] rolls;
    private int currentRoll;
    public Game(){
        rolls = new int[21];
        currentRoll = 0;
    }
    public int getScore() {
        int score = 0;
        for (int i = 0; i < 20; i += 2) {
            int frameScore = rolls[i] + rolls[i + 1];
            if (frameScore == 10 && i < 18){
                frameScore += bonus(i);
            } else if (frameScore >= 10 && i == 18){
                frameScore += rolls[20];
            }
            score += frameScore;
        }

        return score;
    }

    public void roll(int pins) {
        rolls[currentRoll] = pins;
        if((currentRoll % 2 == 0 ) && pins == 10 && currentRoll < 18){
            currentRoll += 2;
        } else currentRoll++;
    }

    public int bonus(int i){
        if (rolls[i] < 10){
            return rolls[i + 2];
        } else if (rolls[i + 2] == 10) {
            return rolls[i + 2] + rolls[i + 4];
        } else {
            return rolls[i + 2] + rolls[i + 3];
        }
    }
}
